package com.example.proyectomovilc4_1artsistasyeventos.view;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.proyectomovilc4_1artsistasyeventos.R;
import com.example.proyectomovilc4_1artsistasyeventos.model.repository.FirebaseAuthRepository;
import com.example.proyectomovilc4_1artsistasyeventos.mvp.ArtistasMVP;
import com.example.proyectomovilc4_1artsistasyeventos.presenter.ArtistasPresenter;
import com.example.proyectomovilc4_1artsistasyeventos.view.adapter.ArtistasAdapter;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.progressindicator.LinearProgressIndicator;

import java.util.List;

public class MainActivity extends AppCompatActivity implements ArtistasMVP.View {
    private DrawerLayout drawerLayout;
    private NavigationView navigationDrawer;

    private LinearProgressIndicator piWaiting;
    private RecyclerView mArtList;
    private ArtistasMVP.Presenter presenter;
    private ArtistasAdapter artistasAdapter;

    //listar artistas Alejandro
    private Button buttonCiudad;
    private Button buttonDepartamento;
    private Button buttonNacionales;
    private Button buttonGenero;
    private Button buttonGps;
    private Button buttonNombreArt;
    private Button buttonRangoPrecio;
    private Button buttonRating;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        presenter = new ArtistasPresenter(this);
        initUI();
        presenter.loadArtistas();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle(R.string.app_name)
                .setMessage("¿Desea cerrar la sesión?")
                .setPositiveButton("Si",
                        (dialog, which) -> {
                            FirebaseAuthRepository.getInstance(MainActivity.this)
                                    .logout(new FirebaseAuthRepository.FirebaseAuthCallback() {
                                        @Override
                                        public void onSuccess() {
                                            MainActivity.super.onBackPressed();
                                            Toast.makeText(MainActivity.this,
                                                    "Sesión Cerrada",
                                                    Toast.LENGTH_SHORT).show();
                                        }

                                        @Override
                                        public void onFailure() {
                                            Toast.makeText(MainActivity.this,
                                                    "Error al cerrar la sesion",
                                                    Toast.LENGTH_SHORT).show();

                                        }
                                    });

                        })
                .setNegativeButton("No", null);

        builder.create().show();
    }

    private void initUI() {
        drawerLayout = findViewById(R.id.drawer_layout);

        MaterialToolbar appBar = findViewById(R.id.app_bar);
        appBar.setNavigationOnClickListener(v -> openDrawer());

        navigationDrawer = findViewById(R.id.navigation_drawer);
        navigationDrawer.setNavigationItemSelectedListener(this::navigationitemSelected);
        piWaiting = findViewById(R.id.pi_waiting);
        mArtList = findViewById(R.id.m_ArtList);
        mArtList.setLayoutManager(new LinearLayoutManager(MainActivity.this));
        artistasAdapter = new ArtistasAdapter();
        artistasAdapter.setOnItemClickListener(presenter::onItemSelected);
        mArtList.setAdapter(artistasAdapter);
        buttonCiudad = findViewById(R.id.bot_mArtCiudad);
        buttonCiudad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //if (view.getId() == buttonCiudad.getId()) {
                //Toast.makeText(getApplicationContext(), "Buscar Artistas Por Ciudad", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(MainActivity.this, FiltroCiudad.class));
                //}
                /*if (view.getId() == buttonDepartamento.getId()) {
                    Toast.makeText(getApplicationContext(), "Buscar Artistas Por Departamento", Toast.LENGTH_SHORT).show();

                }
                if (view.getId() == buttonNacionales.getId()) {
                    Toast.makeText(getApplicationContext(), "Buscar Artistas Por Departamento", Toast.LENGTH_SHORT).show();

                }*/
            }
        });

        buttonDepartamento = findViewById(R.id.bot_mArtDepartamento);
        buttonDepartamento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(getApplicationContext(), "Buscar Artistas Por Departamento", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(MainActivity.this, FiltroDepartamento.class));

            }
        });
        buttonNacionales = findViewById(R.id.bot_mArtNacionales);
        buttonNacionales.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(getApplicationContext(), "Buscar Artistas Nacionales", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(MainActivity.this, FiltroNacionales.class));
            }
        });
        buttonGenero = findViewById(R.id.bot_mArtGenero);
        buttonGenero.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(getApplicationContext(), "Buscar Artistas Por Genero", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(MainActivity.this, FiltroGenero.class));

            }
        });
        buttonGps = findViewById(R.id.bot_mArtCercano);
        buttonGps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(getApplicationContext(), "Buscar Artistas Cercanos (Gps)", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(MainActivity.this, FiltroGps.class));

            }
        });
        buttonNombreArt = findViewById(R.id.bot_mArtNombre);
        buttonNombreArt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(getApplicationContext(), "Buscar Artistas Por Nombre Artistico", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(MainActivity.this, FiltroNombre.class));

            }
        });
        buttonRangoPrecio = findViewById(R.id.bot_mArtPrecios);
        buttonRangoPrecio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(getApplicationContext(), "Buscar Artistas Por Rango de Precio", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(MainActivity.this, FiltroRangoPrecio.class));

            }
        });

        buttonRating=findViewById(R.id.bot_mArtRating);
        buttonRating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getApplicationContext(), "Buscar Artistas Por Rating", Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void openDrawer() {
        drawerLayout.openDrawer(navigationDrawer);

    }

    private boolean navigationitemSelected(MenuItem menuItem) {
        menuItem.setChecked(true);
        Toast.makeText(this, menuItem.getTitle(), Toast.LENGTH_SHORT).show();
        int id = menuItem.getItemId();
        if (id == R.id.menu_search) {
            startActivity(new Intent(MainActivity.this, MainActivity.class));
        } else if (id == R.id.menu_perfilArtista) {
            startActivity(new Intent(MainActivity.this, PerfilArtistaActivity.class));
        } else if (id == R.id.menu_closeSesion) {
            startActivity(new Intent(MainActivity.this, LoginActivity.class));
        } else if (id == R.id.menu_customerS) {
            startActivity(new Intent(MainActivity.this, CustomServiceActivity.class));
        }
        drawerLayout.closeDrawer(navigationDrawer);
        return true;


    }

    @Override
    public Activity getActivity() {
        return MainActivity.this;
    }

    @Override
    public void showProgressBar() {
        piWaiting.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        piWaiting.setVisibility(View.GONE);
    }

    @Override
    public void showArtistasInfo(List<ArtistasMVP.ArtistasInfo> artistasInfo) {
        artistasAdapter.setData(artistasInfo);

        Toast.makeText(MainActivity.this, "Artistas Cargados", Toast.LENGTH_SHORT)
                .show();

    }

    @Override
    public void openLocationActivity(Bundle params) {
        Intent intent = new Intent(MainActivity.this, LocationActivity.class);
        intent.putExtras(params);
        startActivity(intent);
    }
}

