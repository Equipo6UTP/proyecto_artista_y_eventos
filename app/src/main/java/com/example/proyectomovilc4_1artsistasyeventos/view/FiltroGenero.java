package com.example.proyectomovilc4_1artsistasyeventos.view;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.example.proyectomovilc4_1artsistasyeventos.R;
import com.example.proyectomovilc4_1artsistasyeventos.mvp.ArtistasMVP;
import com.example.proyectomovilc4_1artsistasyeventos.presenter.ArtistasPresenter;
import com.example.proyectomovilc4_1artsistasyeventos.view.adapter.ArtistasAdapter;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.progressindicator.LinearProgressIndicator;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class FiltroGenero extends AppCompatActivity implements ArtistasMVP.View{
    private DrawerLayout drawerLayoutGenero;
    private AppCompatSpinner menuGenero;
    private AppCompatButton btnBuscarGenero;

    private NavigationView navigationDrawer;
    private MaterialToolbar appBar;

    private LinearProgressIndicator piWaiting;
    private RecyclerView mArtList;
    private ArtistasMVP.Presenter presenter;
    private ArtistasAdapter artistasAdapter;

    FirebaseDatabase database;
    DatabaseReference databaseReference;
    DatabaseReference generoDatabaseReference;//crear genero


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filtro_genero);
        presenter = new ArtistasPresenter(this);

        database = FirebaseDatabase.getInstance();//ojo min 2:14
        databaseReference = database.getReference("ciudad");//ojo min 3:13
        generoDatabaseReference=database.getReference("genero");//crear genero
        generoDatabaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                //inicio genero

                final List<String> generos = new ArrayList<>();
                for (DataSnapshot generoSnapshot : snapshot.getChildren()) {
                    String generoName = generoSnapshot.child("nombre").getValue(String.class);//5:22
                    generos.add(generoName);//min 5:44
                }

                ArrayAdapter<String> generosAdapter = new ArrayAdapter<String>
                        (FiltroGenero.this, android.R.layout.simple_spinner_item, generos);
                generosAdapter.setDropDownViewResource(android.R.layout.select_dialog_singlechoice);
                menuGenero.setAdapter(generosAdapter);
                menuGenero.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        String item = parent.getSelectedItem().toString();
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });


                //fin genero

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

        initUI();
        presenter.loadArtistas();
    }

    private void openDrawer(){
        drawerLayoutGenero.openDrawer(navigationDrawer);
    }

    private void initUI() {
        drawerLayoutGenero=findViewById(R.id.drawer_layout_genero);

        menuGenero=findViewById(R.id.spinner_genero_musical);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.combo_genero_musical, android.R.layout.simple_spinner_item);
        menuGenero.setAdapter(adapter);

        btnBuscarGenero=findViewById(R.id.btn_buscarGenero);
        btnBuscarGenero.setOnClickListener(v -> buscarArtistasGeneroMusical());

        appBar = findViewById(R.id.app_bar);
        appBar.setNavigationOnClickListener(v -> openDrawer());

        navigationDrawer = findViewById(R.id.navigation_drawer);
        navigationDrawer.setNavigationItemSelectedListener(this::navigationItemSelected);

        piWaiting = findViewById(R.id.pi_waiting);

        mArtList = findViewById(R.id.m_ArtList);
        mArtList.setLayoutManager(new LinearLayoutManager(FiltroGenero.this));

        artistasAdapter = new ArtistasAdapter();
        artistasAdapter.setOnItemClickListener(presenter::onItemSelected);
        mArtList.setAdapter(artistasAdapter);
    }

    private void buscarArtistasGeneroMusical() {
        startActivity(new Intent(FiltroGenero.this, MainActivity.class));
    }

    private boolean navigationItemSelected(MenuItem menuItem) {

        menuItem.setChecked(true);
        Toast.makeText(this,menuItem.getTitle(), Toast.LENGTH_SHORT).show();
        int id = menuItem.getItemId();
        if(id == R.id.menu_search){
            startActivity(new Intent(FiltroGenero.this, MainActivity.class));
        } else if (id == R.id.menu_perfilArtista){
            startActivity(new Intent(FiltroGenero.this, PerfilArtistaActivity.class));
        } else if (id == R.id.menu_closeSesion){
            startActivity(new Intent(FiltroGenero.this, LoginActivity.class));
        } else if (id == R.id.menu_customerS){
            startActivity(new Intent(FiltroGenero.this, CustomServiceActivity.class));
        }
        drawerLayoutGenero.closeDrawer(navigationDrawer);
        return true;
    }

    @Override
    public Activity getActivity() {
        return FiltroGenero.this;
    }

    @Override
    public void showProgressBar() {
        piWaiting.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        piWaiting.setVisibility(View.GONE);
    }

    @Override
    public void showArtistasInfo(List<ArtistasMVP.ArtistasInfo> artistasInfo) {
        artistasAdapter.setData(artistasInfo);

        Toast.makeText(FiltroGenero.this, "Artistas Cargados", Toast.LENGTH_SHORT)
                .show();

    }

    @Override
    public void openLocationActivity(Bundle params) {
        Intent intent = new Intent(FiltroGenero.this, LocationActivity.class);
        intent.putExtras(params);
        startActivity(intent);

    }
}