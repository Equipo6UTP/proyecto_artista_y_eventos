package com.example.proyectomovilc4_1artsistasyeventos.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.example.proyectomovilc4_1artsistasyeventos.R;
import com.example.proyectomovilc4_1artsistasyeventos.mvp.ArtistasMVP;
import com.example.proyectomovilc4_1artsistasyeventos.presenter.ArtistasPresenter;
import com.example.proyectomovilc4_1artsistasyeventos.view.adapter.ArtistasAdapter;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.progressindicator.LinearProgressIndicator;

import java.util.List;

public class FiltroGps extends AppCompatActivity implements ArtistasMVP.View{
    private DrawerLayout drawerLayoutGps;
    private AppCompatButton btnBuscarGps;

    private NavigationView navigationDrawer;
    private MaterialToolbar appBar;

    private LinearProgressIndicator piWaiting;
    private RecyclerView mArtList;
    private ArtistasMVP.Presenter presenter;
    private ArtistasAdapter artistasAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filtro_gps);
        presenter = new ArtistasPresenter(this);

        initUI();
        presenter.loadArtistas();
    }

    private void openDrawer(){
        drawerLayoutGps.openDrawer(navigationDrawer);
    }

    private void initUI() {

        drawerLayoutGps=findViewById(R.id.drawer_layout_gps);

        /*btnBuscarGps=findViewById(R.id.btn_buscarGps);
        btnBuscarGps.setOnClickListener(v -> buscarArtistasPorGps());*/

        appBar = findViewById(R.id.app_bar);
        appBar.setNavigationOnClickListener(v -> openDrawer());

        navigationDrawer = findViewById(R.id.navigation_drawer);
        navigationDrawer.setNavigationItemSelectedListener(this::navigationItemSelected);

        piWaiting = findViewById(R.id.pi_waiting);

        mArtList = findViewById(R.id.m_ArtList);
        mArtList.setLayoutManager(new LinearLayoutManager(FiltroGps.this));

        artistasAdapter = new ArtistasAdapter();
        artistasAdapter.setOnItemClickListener(presenter::onItemSelected);
        mArtList.setAdapter(artistasAdapter);


    }

    /*private void buscarArtistasPorGps() {
        startActivity(new Intent(FiltroGps.this, MainActivity.class));

    }*/

    private boolean navigationItemSelected(MenuItem menuItem) {

        menuItem.setChecked(true);
        Toast.makeText(this,menuItem.getTitle(), Toast.LENGTH_SHORT).show();
        int id = menuItem.getItemId();
        if(id == R.id.menu_search){
            startActivity(new Intent(FiltroGps.this, MainActivity.class));
        } else if (id == R.id.menu_perfilArtista){
            startActivity(new Intent(FiltroGps.this, PerfilArtistaActivity.class));
        } else if (id == R.id.menu_closeSesion){
            startActivity(new Intent(FiltroGps.this, LoginActivity.class));
        } else if (id == R.id.menu_customerS){
            startActivity(new Intent(FiltroGps.this, CustomServiceActivity.class));
        }
        drawerLayoutGps.closeDrawer(navigationDrawer);
        return true;
    }

    @Override
    public Activity getActivity() {
        return FiltroGps.this;
    }

    @Override
    public void showProgressBar() {
        piWaiting.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        piWaiting.setVisibility(View.GONE);
    }

    @Override
    public void showArtistasInfo(List<ArtistasMVP.ArtistasInfo> artistasInfo) {
        artistasAdapter.setData(artistasInfo);

        Toast.makeText(FiltroGps.this, "Artistas Cargados", Toast.LENGTH_SHORT)
                .show();

    }

    @Override
    public void openLocationActivity(Bundle params) {
        Intent intent = new Intent(FiltroGps.this, LocationActivity.class);
        intent.putExtras(params);
        startActivity(intent);

    }
}