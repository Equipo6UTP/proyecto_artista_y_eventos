package com.example.proyectomovilc4_1artsistasyeventos.view;


import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.example.proyectomovilc4_1artsistasyeventos.R;
import com.example.proyectomovilc4_1artsistasyeventos.model.database.entities.User;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class RegistroNuevoArtistaActivity extends AppCompatActivity {
    WebView webView;
    VideoView videoView;

    TextInputEditText etNomArtistico;
    TextInputLayout tilNomArtistico;
    TextInputEditText etGenArt;
    TextInputLayout tilGenArt;
    TextInputEditText etCiuRadicado;
    TextInputLayout tilCiuRadicado;
    TextInputEditText etDepaRadicado;
    TextInputLayout tilDepaRadicado;
    TextInputEditText etCountryRadicado;
    TextInputLayout tilCountryRadicado;
    TextInputEditText etTelContratacion;
    TextInputLayout tilTelContratacion;
    TextInputEditText etEmail;
    TextInputLayout tilEmail;
    TextInputEditText etValorPresentacion;
    TextInputLayout tilValorPresentacion;
    TextInputLayout titRedSocial;
    TextInputEditText etRedSocial;



    AppCompatButton btnContinuar;
    AppCompatButton btnCancelar;
    TextView regEmail;
    DatabaseReference databaseReference;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro_nuevo_artista);
        databaseReference = FirebaseDatabase.getInstance().getReference("user");
        //INSERTO VIDEO EN ARTISTAS
        videoView = (VideoView) findViewById(R.id.videoArtista);
        videoView.setVideoURI(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.tveb));
        MediaController mediaController = new MediaController( this);
        videoView.setMediaController(mediaController);
        videoView.start();
        //INSERTO VIDEO EN ARTISTAS



        initUI();


    }

    private void initUI() {

        etNomArtistico = findViewById(R.id.NomArtistico);
        tilNomArtistico= findViewById(R.id.Nom_Artistico);

        etGenArt = findViewById(R.id.GenerArt);
        tilGenArt = findViewById(R.id.Genero_Art);

        etCiuRadicado = findViewById(R.id.CiudadRadicado);
        tilCiuRadicado= findViewById(R.id.Ciudad_Radicado);

        etDepaRadicado= findViewById(R.id.DepaRadicado);
        tilDepaRadicado= findViewById(R.id.Depa_Radicado);
        etCountryRadicado= findViewById(R.id.CountryRadicado);
        tilCountryRadicado= findViewById(R.id.Country_Radicado);

        etTelContratacion = findViewById(R.id.TelContratacion);
        tilTelContratacion= findViewById(R.id.Tel_Contratacion);

        etEmail = findViewById(R.id.EmailEmpresarial);
        /*etEmail.setText("Ingenieroalexanderlatorre@gmail.com");*/
        tilEmail= findViewById(R.id.Email_Empresarial);


        etValorPresentacion = findViewById(R.id.ValorPresentacion);
        tilValorPresentacion= findViewById(R.id.Valor_Presentacion);

        titRedSocial = findViewById(R.id.tit_RedSocial);
        etRedSocial= findViewById(R.id.et_RedSocial);

        btnContinuar = findViewById(R.id.btn_continuar);
        btnContinuar.setOnClickListener((evt) -> onContinueClick());
        btnCancelar = findViewById(R.id.btn_cancelar);
        btnCancelar.setOnClickListener((evt) -> onCancelClick());
        receivedData();
    }
    public void receivedData() {
        Bundle extras = getIntent().getExtras();
        String e1 = extras.getString("emailShare");
        regEmail= findViewById(R.id.textEmail);
        regEmail.setText(e1);

    }

    private void onContinueClick(){
        /*etEmail.setText("");
        Toast.makeText(this, "", Toast.LENGTH_SHORT).show();*/


        String email = regEmail.getText().toString();
        //User add = new Add(alias,city,department,country,social,price,genre,enterpriseEmail,enterprisePhone);
        String username = email.replace('@','_').replace('.','_');
        databaseReference.child(username).child("alias").setValue(etNomArtistico.getText().toString());
        databaseReference.child(username).child("city").setValue(etCiuRadicado.getText().toString());
        databaseReference.child(username).child("department").setValue(etDepaRadicado.getText().toString());
        databaseReference.child(username).child("country").setValue(etCountryRadicado.getText().toString());
        databaseReference.child(username).child("social").setValue(etRedSocial.getText().toString());
        databaseReference.child(username).child("price").setValue(etValorPresentacion.getText().toString());
        databaseReference.child(username).child("genre").setValue(etGenArt.getText().toString());
        databaseReference.child(username).child("enterpriseEmail").setValue(etEmail.getText().toString());
        databaseReference.child(username).child("enterprisePhone").setValue(etTelContratacion.getText().toString());


        Toast.makeText(getApplicationContext(), "Registro Exitoso", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, PerfilArtistaActivity.class);
        startActivity(intent);

    }
    private void onCancelClick(){
        Intent intent = new Intent(this, RegistroActivity.class);
        startActivity(intent);

    }


}