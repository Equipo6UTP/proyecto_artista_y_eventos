package com.example.proyectomovilc4_1artsistasyeventos.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.example.proyectomovilc4_1artsistasyeventos.R;
import com.example.proyectomovilc4_1artsistasyeventos.mvp.ArtistasMVP;
import com.example.proyectomovilc4_1artsistasyeventos.presenter.ArtistasPresenter;
import com.example.proyectomovilc4_1artsistasyeventos.view.adapter.ArtistasAdapter;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.progressindicator.LinearProgressIndicator;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.util.List;

public class FiltroRangoPrecio extends AppCompatActivity implements ArtistasMVP.View{

    private DrawerLayout drawerLayoutRangoPrecio;

    private TextInputLayout cajaRangoPrecio;

    private AppCompatButton btnBuscarRangoPrecio;

    private NavigationView navigationDrawer;
    private MaterialToolbar appBar;

    private LinearProgressIndicator piWaiting;
    private RecyclerView mArtList;
    private ArtistasMVP.Presenter presenter;
    private ArtistasAdapter artistasAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filtro_rango_precio);
        presenter = new ArtistasPresenter(this);

        initUI();
        presenter.loadArtistas();
    }

    private void openDrawer(){
        drawerLayoutRangoPrecio.openDrawer(navigationDrawer);
    }

    private void initUI() {
        drawerLayoutRangoPrecio=findViewById(R.id.drawer_layout_rango_precio);

        cajaRangoPrecio=findViewById(R.id.busqueda_rango_precio);

        btnBuscarRangoPrecio=findViewById(R.id.btn_rango_precio);
        btnBuscarRangoPrecio.setOnClickListener(v -> buscarArtistasPorRangoPrecio());

        appBar = findViewById(R.id.app_bar);
        appBar.setNavigationOnClickListener(v -> openDrawer());

        navigationDrawer = findViewById(R.id.navigation_drawer);
        navigationDrawer.setNavigationItemSelectedListener(this::navigationItemSelected);

        piWaiting = findViewById(R.id.pi_waiting);

        mArtList = findViewById(R.id.m_ArtList);
        mArtList.setLayoutManager(new LinearLayoutManager(FiltroRangoPrecio.this));

        artistasAdapter = new ArtistasAdapter();
        artistasAdapter.setOnItemClickListener(presenter::onItemSelected);
        mArtList.setAdapter(artistasAdapter);
    }

    private void buscarArtistasPorRangoPrecio() {
        startActivity(new Intent(FiltroRangoPrecio.this, MainActivity.class));

    }

    private boolean navigationItemSelected(MenuItem menuItem) {

        menuItem.setChecked(true);
        Toast.makeText(this,menuItem.getTitle(), Toast.LENGTH_SHORT).show();
        int id = menuItem.getItemId();
        if(id == R.id.menu_search){
            startActivity(new Intent(FiltroRangoPrecio.this, MainActivity.class));
        } else if (id == R.id.menu_perfilArtista){
            startActivity(new Intent(FiltroRangoPrecio.this, PerfilArtistaActivity.class));
        } else if (id == R.id.menu_closeSesion){
            startActivity(new Intent(FiltroRangoPrecio.this, LoginActivity.class));
        } else if (id == R.id.menu_customerS){
            startActivity(new Intent(FiltroRangoPrecio.this, CustomServiceActivity.class));
        }
        drawerLayoutRangoPrecio.closeDrawer(navigationDrawer);
        return true;
    }

    @Override
    public Activity getActivity() {
        return FiltroRangoPrecio.this;
    }

    @Override
    public void showProgressBar() {
        piWaiting.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        piWaiting.setVisibility(View.GONE);
    }

    @Override
    public void showArtistasInfo(List<ArtistasMVP.ArtistasInfo> artistasInfo) {
        artistasAdapter.setData(artistasInfo);

        Toast.makeText(FiltroRangoPrecio.this, "Artistas Cargados", Toast.LENGTH_SHORT)
                .show();

    }

    @Override
    public void openLocationActivity(Bundle params) {
        Intent intent = new Intent(FiltroRangoPrecio.this, LocationActivity.class);
        intent.putExtras(params);
        startActivity(intent);

    }
}