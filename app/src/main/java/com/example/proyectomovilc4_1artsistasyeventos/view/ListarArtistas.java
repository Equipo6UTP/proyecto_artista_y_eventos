package com.example.proyectomovilc4_1artsistasyeventos.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;

import com.example.proyectomovilc4_1artsistasyeventos.R;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;


public class ListarArtistas extends AppCompatActivity {

    //creo las variables del menu
    private DrawerLayout drawerLayout;//de tipo DrawerLayout (activity_listar_artistas)
    private MaterialToolbar appBar;//de tipo   MaterialToolbar (activity_listar_artistas)
    private NavigationView navigationDrawer;

    private FloatingActionButton btnFlotante; //boton flotante
    //fin variables menu

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listar_artistas);

        initUI();//inicializar el metodo
    }

    //inicializar los campos del menu desde este metodo
    private void initUI() {
        drawerLayout = findViewById(R.id.drawer_layout);

        appBar = findViewById(R.id.app_bar);
        appBar.setNavigationOnClickListener(v -> openDrawer());

        navigationDrawer = findViewById(R.id.navigation_drawer);
        navigationDrawer.setNavigationItemSelectedListener(this::navigationItemSelected);

        btnFlotante=findViewById(R.id.btn_sale);
        btnFlotante.setOnClickListener(v -> onNewContacto());//evento boton flotante
    }
    //metodo del boton flotante
    private void onNewContacto() {
        startActivity(new Intent(ListarArtistas.this, CustomServiceActivity.class));
    }

    //metodo menu
    private void openDrawer() {

        drawerLayout.openDrawer(navigationDrawer);
    }
    //metodo menu
    private boolean navigationItemSelected(MenuItem menuItem) {
        /*
        menuItem.setChecked(true);
        Toast.makeText(this, menuItem.getTitle(), Toast.LENGTH_SHORT).show();//quemar datos muestra en cada link a lo que correspone
        drawerLayout.closeDrawer(navigationDrawer);
        return true;
        */


        menuItem.setChecked(true);
        Toast.makeText(this,menuItem.getTitle(), Toast.LENGTH_SHORT).show();
        int id = menuItem.getItemId();
        if(id == R.id.menu_search){
            startActivity(new Intent(ListarArtistas.this, ListarArtistas.class));
        } else if (id == R.id.menu_perfilArtista){
            startActivity(new Intent(ListarArtistas.this, PerfilArtistaActivity.class));
        } else if (id == R.id.menu_closeSesion){
            startActivity(new Intent(ListarArtistas.this, LoginActivity.class));
        } else if (id == R.id.menu_customerS){
            startActivity(new Intent(ListarArtistas.this, CustomServiceActivity.class));
        }
        drawerLayout.closeDrawer(navigationDrawer);
        return true;
    }
}