package com.example.proyectomovilc4_1artsistasyeventos.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.drawerlayout.widget.DrawerLayout;

import com.example.proyectomovilc4_1artsistasyeventos.R;
import com.example.proyectomovilc4_1artsistasyeventos.mvp.LoginMVP;
import com.example.proyectomovilc4_1artsistasyeventos.presenter.LoginPresenter;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.progressindicator.LinearProgressIndicator;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

public class LoginActivity extends AppCompatActivity implements LoginMVP.View {

    //private DrawerLayout drawerLayout;
    //private NavigationView navigationDrawer;
    //private MaterialToolbar appBar;


    //creo la interfaz grafica en variables para ser manipuladas con java
    private LinearProgressIndicator piWaiting;

    private ImageView logoPrincipal;
    private TextInputLayout titleEmail;
    private TextInputEditText email;
    private TextInputLayout titlePassword;
    private TextInputEditText password;
    private AppCompatTextView appCompatTextView;

    private AppCompatButton btnStar;
    private AppCompatButton btnFacebook;
    private AppCompatButton btnGoogle;

    private LoginMVP.Presenter presenter;//me trae la interface Presenter

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        presenter = new LoginPresenter(this);//llama la vista //necesito un presentador alguien que gestione los eventos
        presenter.isAuthenticated();

        initUI();
    }


    //Inicializo las variables
    private void initUI() {

        piWaiting= findViewById(R.id.pi_waiting);

        logoPrincipal = findViewById(R.id.logo_principal);

        titleEmail = findViewById(R.id.title_email);
        email = findViewById(R.id.email);

        titlePassword = findViewById(R.id.title_password);
        password = findViewById(R.id.password);

        appCompatTextView = findViewById(R.id.register);
        appCompatTextView.setOnClickListener(evt -> register());

        btnStar = findViewById(R.id.btn_star);
        btnStar.setOnClickListener(evt -> presenter.loginWithEmail());//llamamos el metodo de presenter

        btnFacebook = findViewById(R.id.btn_facebook);
        btnFacebook.setOnClickListener(evt -> presenter.loginWithFacebook());//llamamos el metodo de presenter

        btnGoogle = findViewById(R.id.btn_google);
        btnGoogle.setOnClickListener(evt -> presenter.loginWithGoogle());//llamamos el metodo de presenter


        //drawerLayout = findViewById(R.id.drawer_layout);

        //appBar = findViewById(R.id.app_bar);
//        appBar.setNavigationOnClickListener(v -> openDrawer());

        //navigationDrawer = findViewById(R.id.navigation_drawer);
//        navigationDrawer.setNavigationItemSelectedListener(this::navigationItemSelected);


    }


    /*private void openDrawer() {

        drawerLayout.openDrawer(navigationDrawer);
    }*/


    @Override
    public Activity getActivity() {
        return this;
    }

    @Override
    public LoginMVP.LoginInfo getLoginInfo() {
        return new LoginMVP.LoginInfo(email.getText().toString(), password.getText().toString());
    }

    @Override
    public void showEmailError(String error) {
        titleEmail.setError(error);
    }

    @Override
    public void showPasswordError(String error) {
        titlePassword.setError(error);

    }

    @Override
    public void showGeneralMessage(String error) {
        Toast.makeText(LoginActivity.this, error, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void clearDatos() {
        titleEmail.setError("");
        email.setText("");
        titlePassword.setError("");
        password.setText("");
    }

    @Override
    public void openNewActivity() {
        Intent intent = new Intent(this, MainActivity.class);//ventana donde va a saltar
        startActivity(intent);//variable que contiene la ventana a ejecutar


    }
    @Override
    public void register() {
        Intent intent = new Intent(this, RegistroActivity.class);//ventana donde va a saltar
        startActivity(intent);//variable que contiene la ventana a ejecutar
    }

    @Override
    public void startWaiting() {
        piWaiting.setVisibility(View.VISIBLE);
    }

    @Override
    public void stopWaiting() {
        piWaiting.setVisibility(View.GONE);

    }


    /*private boolean navigationItemSelected(MenuItem menuItem) {

        menuItem.setChecked(true);
        Toast.makeText(this,menuItem.getTitle(), Toast.LENGTH_SHORT).show();
        int id = menuItem.getItemId();
        if(id == R.id.menu_search){
            startActivity(new Intent(LoginActivity.this, MainActivity.class));
        } else if (id == R.id.menu_register){
            startActivity(new Intent(LoginActivity.this, RegistroActivity.class));
        } else if (id == R.id.menu_initSesion){
            startActivity(new Intent(LoginActivity.this, LoginActivity.class));
        } else if (id == R.id.menu_customerS){
            startActivity(new Intent(LoginActivity.this, CustomServiceActivity.class));
        }
        drawerLayout.closeDrawer(navigationDrawer);
        return true;
    }*/


}