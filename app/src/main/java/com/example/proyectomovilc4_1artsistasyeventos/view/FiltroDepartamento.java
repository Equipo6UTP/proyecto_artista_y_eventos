package com.example.proyectomovilc4_1artsistasyeventos.view;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.example.proyectomovilc4_1artsistasyeventos.R;
import com.example.proyectomovilc4_1artsistasyeventos.mvp.ArtistasMVP;
import com.example.proyectomovilc4_1artsistasyeventos.presenter.ArtistasPresenter;
import com.example.proyectomovilc4_1artsistasyeventos.view.adapter.ArtistasAdapter;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.progressindicator.LinearProgressIndicator;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class FiltroDepartamento extends AppCompatActivity implements ArtistasMVP.View{

    private DrawerLayout drawerLayoutDepartamento;
    private AppCompatSpinner menuDepartamento;
    private AppCompatButton btnBuscarDepartamento;
    private AppCompatSpinner menuGeneroDepartamento;

    private NavigationView navigationDrawer;
    private MaterialToolbar appBar;

    private LinearProgressIndicator piWaiting;
    private RecyclerView mArtList;
    private ArtistasMVP.Presenter presenter;
    private ArtistasAdapter artistasAdapter;

    FirebaseDatabase database;
    DatabaseReference databaseReference;
    DatabaseReference generoDatabaseReference;//crear genero

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filtro_departamento);
        presenter = new ArtistasPresenter(this);


        //inicio

        database = FirebaseDatabase.getInstance();//ojo min 2:14
        databaseReference = database.getReference("departamento");//ojo min 3:13
        generoDatabaseReference=database.getReference("genero");//crear genero
        generoDatabaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                //inicio genero

                final List<String> generos = new ArrayList<>();
                for (DataSnapshot generoSnapshot : snapshot.getChildren()) {
                    String generoName = generoSnapshot.child("nombre").getValue(String.class);//5:22
                    generos.add(generoName);//min 5:44
                }

                ArrayAdapter<String> generosAdapter = new ArrayAdapter<String>
                        (FiltroDepartamento.this, android.R.layout.simple_spinner_item, generos);
                generosAdapter.setDropDownViewResource(android.R.layout.select_dialog_singlechoice);
                menuGeneroDepartamento.setAdapter(generosAdapter);
                menuGeneroDepartamento.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        String item = parent.getSelectedItem().toString();
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });


                //fin genero

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
        databaseReference.addValueEventListener(new ValueEventListener() {


            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {

                final List<String> departamentos = new ArrayList<>();
                for (DataSnapshot departamentoSnapshot : snapshot.getChildren()) {
                    String departamentoName = departamentoSnapshot.child("nombre").getValue(String.class);//5:22
                    departamentos.add(departamentoName);//min 5:44
                }

                ArrayAdapter<String> departamentosAdapter = new ArrayAdapter<String>
                        (FiltroDepartamento.this, android.R.layout.simple_spinner_item, departamentos);
                departamentosAdapter.setDropDownViewResource(android.R.layout.select_dialog_singlechoice);
                menuDepartamento.setAdapter(departamentosAdapter);
                menuDepartamento.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        String item = parent.getSelectedItem().toString();
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });



            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });


        //fin

        initUI();
        presenter.loadArtistas();
    }

    private void openDrawer(){
        drawerLayoutDepartamento.openDrawer(navigationDrawer);
    }

    private void initUI() {
        drawerLayoutDepartamento=findViewById(R.id.drawer_layout_departamento);

        menuDepartamento = findViewById(R.id.spinner_departamento);
        /*ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.combo_departamento, android.R.layout.simple_spinner_item);
        menuDepartamento.setAdapter(adapter);
*/

        menuGeneroDepartamento=findViewById(R.id.spinner_departamento_genero);
        /*ArrayAdapter<CharSequence> adapterGenero = ArrayAdapter.createFromResource(this,
                R.array.combo_genero_departamento, android.R.layout.simple_spinner_item);
        menuGeneroDepartamento.setAdapter(adapterGenero);*/


        btnBuscarDepartamento=findViewById(R.id.btn_buscarDepartamento);
        btnBuscarDepartamento.setOnClickListener(v -> buscarArtistasDepartamento());

        appBar = findViewById(R.id.app_bar);
        appBar.setNavigationOnClickListener(v -> openDrawer());

        navigationDrawer = findViewById(R.id.navigation_drawer);
        navigationDrawer.setNavigationItemSelectedListener(this::navigationItemSelected);

        piWaiting = findViewById(R.id.pi_waiting);

        mArtList = findViewById(R.id.m_ArtList);
        mArtList.setLayoutManager(new LinearLayoutManager(FiltroDepartamento.this));

        artistasAdapter = new ArtistasAdapter();
        artistasAdapter.setOnItemClickListener(presenter::onItemSelected);
        mArtList.setAdapter(artistasAdapter);

    }

    private void buscarArtistasDepartamento() {
        startActivity(new Intent(FiltroDepartamento.this, MainActivity.class));

    }

    private boolean navigationItemSelected(MenuItem menuItem) {

        menuItem.setChecked(true);
        Toast.makeText(this,menuItem.getTitle(), Toast.LENGTH_SHORT).show();
        int id = menuItem.getItemId();
        if(id == R.id.menu_search){
            startActivity(new Intent(FiltroDepartamento.this, MainActivity.class));
        } else if (id == R.id.menu_perfilArtista){
            startActivity(new Intent(FiltroDepartamento.this, PerfilArtistaActivity.class));
        } else if (id == R.id.menu_closeSesion){
            startActivity(new Intent(FiltroDepartamento.this, LoginActivity.class));
        } else if (id == R.id.menu_customerS){
            startActivity(new Intent(FiltroDepartamento.this, CustomServiceActivity.class));
        }
        drawerLayoutDepartamento.closeDrawer(navigationDrawer);
        return true;
    }


    @Override
    public Activity getActivity() {
        return FiltroDepartamento.this;
    }

    @Override
    public void showProgressBar() {
        piWaiting.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        piWaiting.setVisibility(View.GONE);
    }

    @Override
    public void showArtistasInfo(List<ArtistasMVP.ArtistasInfo> artistasInfo) {
        artistasAdapter.setData(artistasInfo);

        Toast.makeText(FiltroDepartamento.this, "Artistas Cargados", Toast.LENGTH_SHORT)
                .show();

    }

    @Override
    public void openLocationActivity(Bundle params) {
        Intent intent = new Intent(FiltroDepartamento.this, LocationActivity.class);
        intent.putExtras(params);
        startActivity(intent);

    }
}