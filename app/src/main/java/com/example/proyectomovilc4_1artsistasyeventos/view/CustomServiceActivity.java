package com.example.proyectomovilc4_1artsistasyeventos.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.drawerlayout.widget.DrawerLayout;

import com.example.proyectomovilc4_1artsistasyeventos.R;
import com.example.proyectomovilc4_1artsistasyeventos.model.database.entities.User;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class CustomServiceActivity extends AppCompatActivity {

    AppCompatButton botServEnvio;
    TextInputLayout tilServNombre;
    TextInputEditText etServNombre;
    TextInputLayout tilServTelefono;
    TextInputEditText etServTelefono;
    TextInputLayout tilServCorreoE;
    TextInputEditText etServCorreoE;
    TextInputLayout tilServMensaje;
    TextInputEditText etServMensaje;
    ImageView ivLogo;
    DatabaseReference databaseReference;

    private DrawerLayout drawerLayout;
    private NavigationView navigationDrawer;
    private MaterialToolbar appBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_service);
        databaseReference = FirebaseDatabase.getInstance().getReference();

        initUI();
    }

    private void openDrawer(){
        drawerLayout.openDrawer(navigationDrawer);

    }

    private void initUI() {

        // inicializar los elementos

        ivLogo = findViewById(R.id.logo_principal);

        tilServNombre =findViewById(R.id.tit_ServNombre);
        etServNombre = findViewById(R.id.input_ServNombre);

        tilServTelefono =findViewById(R.id.tit_servTelefono);
        etServTelefono = findViewById(R.id.input_servTelefono);

        tilServCorreoE  =findViewById(R.id.tit_servCorreoE );
        etServCorreoE = findViewById(R.id.input_servCorreoE);

        tilServMensaje =findViewById(R.id.tit_servMensaje);
        etServMensaje = findViewById(R.id.input_servMensaje);


        botServEnvio = findViewById(R.id.bot_servEnvio);
        botServEnvio.setOnClickListener((evt)-> onClickServEnvio());


        drawerLayout = findViewById(R.id.drawer_layout);

        appBar = findViewById(R.id.app_bar);
        appBar.setNavigationOnClickListener(v -> openDrawer());

        navigationDrawer = findViewById(R.id.navigation_drawer);
        navigationDrawer.setNavigationItemSelectedListener(this::navigationItemSelected);

    }


    private void onClickServEnvio(){
        //etServNombre.setText("");
        //etServTelefono.setText("");
        //etServMensaje.setText("");
        
        String name = etServNombre.getText().toString().trim();
        String phone = etServTelefono.getText().toString().trim();
        String email = etServCorreoE.getText().toString().trim();
        String message = etServMensaje.getText().toString().trim();  
        
        User user = new User(name,phone,email,message);
        String mensaje = email.replace('@','_').replace('.','_');
        databaseReference.child("user").child(mensaje).setValue(user);
        Toast.makeText(this,"Comentario Enviado" , Toast.LENGTH_SHORT).show();
        Intent intent =new Intent(this, LoginActivity.class);
        startActivity(intent);

    }

    private boolean navigationItemSelected(MenuItem menuItem) {

        menuItem.setChecked(true);
        Toast.makeText(this,menuItem.getTitle(), Toast.LENGTH_SHORT).show();
        int id = menuItem.getItemId();
        if(id == R.id.menu_search){
            startActivity(new Intent(CustomServiceActivity.this, MainActivity.class));
        } else if (id == R.id.menu_perfilArtista){
            startActivity(new Intent(CustomServiceActivity.this, PerfilArtistaActivity.class));
        } else if (id == R.id.menu_closeSesion){
            startActivity(new Intent(CustomServiceActivity.this, LoginActivity.class));
        } else if (id == R.id.menu_customerS){
            startActivity(new Intent(CustomServiceActivity.this, CustomServiceActivity.class));
        }
        drawerLayout.closeDrawer(navigationDrawer);
        return true;
    }



}