package com.example.proyectomovilc4_1artsistasyeventos.view.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.proyectomovilc4_1artsistasyeventos.R;
import com.example.proyectomovilc4_1artsistasyeventos.mvp.ArtistasMVP;


import java.util.ArrayList;
import java.util.List;

public class ArtistasAdapter extends RecyclerView.Adapter<ArtistasAdapter.ViewHolder> {
    private List<ArtistasMVP.ArtistasInfo> data;
    //MAPS
    private onItemClickListener onItemClickListener;
    //MAPS
    public ArtistasAdapter(){
        this.data=new ArrayList<>();
    }

    public void setData(List<ArtistasMVP.ArtistasInfo> data) {
        this.data = data;
        notifyDataSetChanged();
    }
    //MAPS
    public void setOnItemClickListener(onItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }
    //MAPS

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_artista, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ArtistasMVP.ArtistasInfo item = data.get(position);

        //MAPS
        //TODO asignar Click Listener o Elemento
        if (onItemClickListener !=null) {
            holder.itemView.setOnClickListener(v -> onItemClickListener.onItemClick(item));

        }
        //MAPS
        //TODO holder.getCicLArtImage().setImageIcon();
        holder.getCtitLArtNombre().setText(item.getName());
        holder.getCtitLArtCiudad().setText(item.getCity());
        holder.getCtitLArtGenero().setText(item.getGenre());
        holder.getCtitLArtVerificado().setText(item.getVerified());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }



    public static class ViewHolder extends RecyclerView.ViewHolder{
        private ImageView cicLArtImage;
        private TextView ctitLArtNombre;
        private TextView ctitLArtGenero;
        private TextView ctitLArtCiudad;
        private TextView ctitLArtVerificado;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            cicLArtImage=itemView.findViewById(R.id.c_ic_LArtImage);
            ctitLArtNombre=itemView.findViewById(R.id.c_tit_LArtNombre);
            ctitLArtGenero=itemView.findViewById(R.id.c_tit_LArtGenero);
            ctitLArtCiudad=itemView.findViewById(R.id.c_tit_LArtCiudad);
            ctitLArtVerificado=itemView.findViewById(R.id.c_tit_LArtVerificado);

        }



        public ImageView getCicLArtImage() {
            return cicLArtImage;
        }

        public TextView getCtitLArtNombre() {
            return ctitLArtNombre;
        }

        public TextView getCtitLArtGenero() {
            return ctitLArtGenero;
        }

        public TextView getCtitLArtCiudad() {
            return ctitLArtCiudad;
        }

        public TextView getCtitLArtVerificado() {
            return ctitLArtVerificado;
        }
    }
    // MAPS
    public interface onItemClickListener {
    void onItemClick(ArtistasMVP.ArtistasInfo info);
    }
    // MAPS
}
