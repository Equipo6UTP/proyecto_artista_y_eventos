package com.example.proyectomovilc4_1artsistasyeventos.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.drawerlayout.widget.DrawerLayout;

import com.example.proyectomovilc4_1artsistasyeventos.R;
import com.example.proyectomovilc4_1artsistasyeventos.model.database.entities.User;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class RegistroActivity extends AppCompatActivity {

    /*private DrawerLayout drawerLayout ;
    private NavigationView navigationDrawer;
    private MaterialToolbar appBar;
*/

    RadioGroup gr_botRegistro;
    RadioButton botCliente;
    RadioButton botartista;

    TextInputLayout titRegNombre;
    TextInputEditText inputRegNombre;

    TextInputLayout titRegApellido;
    TextInputEditText inputRegApellido;

    TextInputLayout titRegTelefono;
    TextInputEditText inputRegTelefono;

    TextInputLayout titRegCorreoE;
    TextInputEditText inputRegCorreoE;

    TextInputLayout titRegContrasena;
    TextInputEditText inputRegContrasena;

    TextInputLayout titRegContrasena2;
    TextInputEditText inputRegContrasena2;

    AppCompatButton botRegistro;

    AppCompatTextView appCompatTextView;

    DatabaseReference databaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);
        databaseReference = FirebaseDatabase.getInstance().getReference();
        initUI();

    }

    /*private void openDrawer(){
        drawerLayout.openDrawer(navigationDrawer);

    }*/



    private void initUI() {

        /*drawerLayout = findViewById(R.id.drawer_layout);

        appBar = findViewById(R.id.app_bar);
        appBar.setNavigationOnClickListener(v -> openDrawer());

        navigationDrawer = findViewById(R.id.navigation_drawer);
        navigationDrawer.setNavigationItemSelectedListener(this::navigationItemSelected);*/


        gr_botRegistro = findViewById(R.id.gr_bot_registro);
        botCliente = findViewById(R.id.bot_Cliente);
        botartista = findViewById(R.id.bot_artista);

        titRegNombre = findViewById(R.id.tit_regNombre);
        inputRegNombre = findViewById(R.id.input_regNombre);

        titRegApellido = findViewById(R.id.tit_regApellido);
        inputRegApellido = findViewById(R.id.input_regApellido);

        titRegTelefono = findViewById(R.id.tit_regTelefono);
        inputRegTelefono = findViewById(R.id.input_regTelefono);

        titRegCorreoE = findViewById(R.id.tit_regCorreoE);
        inputRegCorreoE = findViewById(R.id.input_regCorreoE);

        titRegContrasena = findViewById(R.id.tit_regContrasena);
        inputRegContrasena = findViewById(R.id.input_regContrasena);

        titRegContrasena2 = findViewById(R.id.tit_regContrasena2);
        inputRegContrasena2 = findViewById(R.id.input_regContrasena2);

        botRegistro=findViewById(R.id.bot_registro);
        botRegistro.setOnClickListener((evt) -> { onRegisterClick(); });

        appCompatTextView=findViewById(R.id.text_regIngreso);
        appCompatTextView.setOnClickListener(evt ->{
            login();
        });
    }



    /*private boolean navigationItemSelected(MenuItem menuItem) {

        menuItem.setChecked(true);
        Toast.makeText(this,menuItem.getTitle(), Toast.LENGTH_SHORT).show();
        int id = menuItem.getItemId();
        if(id == R.id.menu_search){
            startActivity(new Intent(RegistroActivity.this, MainActivity.class));
        } else if (id == R.id.menu_register){
            startActivity(new Intent(RegistroActivity.this, RegistroActivity.class));
        } else if (id == R.id.menu_initSesion){
            startActivity(new Intent(RegistroActivity.this, LoginActivity.class));
        } else if (id == R.id.menu_customerS){
            startActivity(new Intent(RegistroActivity.this, CustomServiceActivity.class));
        }
//        drawerLayout.closeDrawer(navigationDrawer);
        return true;
    }*/



    private void onRegisterClick(){

        if (botCliente.isChecked()){
            String name = inputRegNombre.getText().toString().trim();
            String lastname = inputRegApellido.getText().toString().trim();
            String phone = inputRegTelefono.getText().toString().trim();
            String email = inputRegCorreoE.getText().toString().trim();
            String password = inputRegContrasena.getText().toString().trim();
            String type = botCliente.getText().toString().trim();
            botartista.setChecked(false);

            //String image = inputRegNombre.getText().toString().trim();
            /*Boolean enable= inputRegNombre.getText().toString().trim();
            Boolean verified = inputRegNombre.getText().toString().trim();
            String genre= inputRegNombre.getText().toString().trim();
            String alias= inputRegNombre.getText().toString().trim();
            String city= inputRegNombre.getText().toString().trim();
            String department= inputRegNombre.getText().toString().trim();
            String country= inputRegNombre.getText().toString().trim();
            String video= inputRegNombre.getText().toString().trim();
            String price= inputRegNombre.getText().toString().trim();
            String rating= inputRegNombre.getText().toString().trim();
            String type= inputRegNombre.getText().toString().trim();*/

            User user = new User(name,lastname,phone,email,password,type);
            String username = email.replace('@','_').replace('.','_');
            databaseReference.child("user").child(username).setValue(user);
            Toast.makeText(this,"Registro exitoso", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);

        }else if (botartista.isChecked()){
            botCliente.setChecked(false);
            String name = inputRegNombre.getText().toString().trim();
            String lastname = inputRegApellido.getText().toString().trim();
            String phone = inputRegTelefono.getText().toString().trim();
            String email = inputRegCorreoE.getText().toString().trim();
            String password = inputRegContrasena.getText().toString().trim();
            String type = botartista.getText().toString().trim();
            User user = new User(name,lastname,phone,email,password,type);
            String username = email.replace('@','_').replace('.','_');
            databaseReference.child("user").child(username).setValue(user);
            Intent intent = new Intent(this, RegistroNuevoArtistaActivity.class);
            intent.putExtra("emailShare",email);
            startActivity(intent);

        }else{
            Toast.makeText(getApplicationContext(), "¿Eres Cliente o Artista?", Toast.LENGTH_SHORT).show();
        }
    }

    private void login() {
        Intent intent = new Intent(this, LoginActivity.class);//ventana donde va a saltar
        startActivity(intent);//variable que contiene la ventana a ejecutar
    }
}