package com.example.proyectomovilc4_1artsistasyeventos.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentActivity;

import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;

import com.example.proyectomovilc4_1artsistasyeventos.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;
import java.util.Locale;
/*import com.example.proyectomovilc4_1artsistasyeventos.view.databinding.ActivityLocationBinding;*/

public class LocationActivity extends AppCompatActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
/*    private ActivityLocationBinding binding;*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

 /*       binding = ActivityLocationBinding.inflate(getLayoutInflater());*/
        setContentView(R.layout.activity_location);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setZoomControlsEnabled(true);
        //MAPS
        String name = getIntent().getStringExtra("name");
        String city = getIntent().getStringExtra("city");
        //MAPS

            LatLng location = new LatLng(4, -72);
        try {
            Geocoder geo = new Geocoder(LocationActivity.this.getApplicationContext(), Locale.getDefault());
            List<Address> addresses = geo.getFromLocationName(city, 1);
            if (!addresses.isEmpty()) {
                location = new LatLng(addresses.get(0).getLatitude(),
                        addresses.get(0).getLongitude());
            }
        } catch (Exception e) {
            e.printStackTrace(); // getFromLocation() may sometimes fail
        }
        // Add a marker in Sydney and move the camera
        mMap.addMarker(new MarkerOptions()
                .position(location)
                .title(name + " - " + city));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(location, 14f));
    }
}