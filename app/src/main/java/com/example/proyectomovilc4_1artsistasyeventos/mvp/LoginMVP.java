package com.example.proyectomovilc4_1artsistasyeventos.mvp;

//creamos las 3 interfaces(Model, Presenter, View)

import android.app.Activity;
import android.content.Context;

public class LoginMVP {


    public interface Model {
        void validateCredentials(String email, String password, ValidateCredentialsCallback callback);

        boolean isAuthenticated();

        interface ValidateCredentialsCallback{
            void onSuccess();
            void onFailure(String error);

        }


    }
    //Aca creamos los eventos que contienen la ventanas de diseño en este caso activity_login.xml
    //Boton Iniciar sesión, boton facebook y boton google o los que tengamos
    public interface Presenter {
        void loginWithEmail();//metodo captura el evento del boton iniciar sesion

        void loginWithFacebook();//metodo captura el evento del boton Facebook

        void loginWithGoogle();//metodo captura el evento del boton Google


        void isAuthenticated();
    }

    //capturar los datos que ingresa el usuario en el formulario activity_login.xml
    public interface View {
        Activity getActivity();

        LoginInfo getLoginInfo();//vista le pido los datos

        void showEmailError(String error);//metodo con un mensaje de error en el email
        void showPasswordError(String error);//metodo un mensaje de error en el password
        void showGeneralMessage(String error);//mensaje no pudo logearse

        void clearDatos();//metodo para limpiar campos

        void openNewActivity();//metodo para Abrir una nueva ventana (Enlace, link)(Activity)
        void register();

        void startWaiting();
        void stopWaiting();
    }

    //creo la clase y las variables que contendra los datos email y password que digite el usuario
    public static class LoginInfo {
        private String email;
        private String password;

        //creo el constructor con los 2 parametros
        public LoginInfo(String email, String password) {
            this.email = email;
            this.password = password;
        }
        //creo los metodos get al email y password

        public String getEmail() {
            return email;
        }

        public String getPassword() {
            return password;
        }
    }

}
