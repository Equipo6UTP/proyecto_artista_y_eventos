package com.example.proyectomovilc4_1artsistasyeventos.mvp;

import android.app.Activity;
import android.os.Bundle;

import java.util.List;

public interface ArtistasMVP {

    interface Model{
        void loadArtistas(loadPaymentsCallback callback);

        interface loadPaymentsCallback{


            void showArtistasInfo(List<ArtistasInfo> artistasInfo);

        }

    }
    interface Presenter{
        void loadArtistas();


        void onItemSelected(ArtistasInfo info);
    }
    interface View {
        Activity getActivity();

        void showProgressBar();
        void hideProgressBar();

        void showArtistasInfo(List<ArtistasInfo> artistasInfo);

        void openLocationActivity(Bundle params);
    }
    class ArtistasInfo{
        private String image;
        private String name;
        private String genre;
        private String city;
        private String verified;

        public ArtistasInfo(String name, String genre, String city, String verified) {
            this(null, name, genre,city,verified );
        }

        public ArtistasInfo(String image, String name, String genre, String city, String verified) {
            this.image = image;
            this.name = name;
            this.genre = genre;
            this.city = city;
            this.verified = verified;
        }



        public String getImage() {
            return image;
        }

        public String getName() {
            return name;
        }

        public String getGenre() {
            return genre;
        }

        public String getCity() {
            return city;
        }

        public String getVerified() {
            return verified;
        }
    }
}
