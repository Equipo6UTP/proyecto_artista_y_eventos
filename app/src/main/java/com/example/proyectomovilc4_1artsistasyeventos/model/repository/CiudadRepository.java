package com.example.proyectomovilc4_1artsistasyeventos.model.repository;

import android.content.Context;
import android.util.Log;

import com.example.proyectomovilc4_1artsistasyeventos.model.database.ArtistasDatabase;
import com.example.proyectomovilc4_1artsistasyeventos.model.database.dao.CiudadDao;
import com.example.proyectomovilc4_1artsistasyeventos.model.database.dao.UserDao;
import com.example.proyectomovilc4_1artsistasyeventos.model.database.entities.Ciudad;
import com.example.proyectomovilc4_1artsistasyeventos.model.database.entities.User;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

public class CiudadRepository {

    private static CiudadRepository instance;

    public static CiudadRepository getInstance(Context context){
        if (instance == null){
            instance = new CiudadRepository(context);
        }
        return instance;
    }

    private final static Boolean USE_DATABASE = Boolean.FALSE;

    // determinar de donde se va a obtener la info
    private CiudadDao ciudadDao;

    private DatabaseReference ciudadRef;

    //private UserDao userRef;


    private CiudadRepository(Context context) {
        ciudadDao = ArtistasDatabase.getDatabase(context).getCiudadDao();

        //Conexión a base de datos
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        ciudadRef= database.getReference("ciudad");

    }

    /*private User getUserByEmailDB(String email) {
        return userDao.getUserByEmail(email);
    }*/

    public void getAll(CiudadRepository.CiudadesCallback<List<Ciudad>> callback){
        ciudadRef.get().addOnCompleteListener(task -> {
            if(task.isSuccessful()){
                DataSnapshot dataSnapshot = task.getResult();
                if(dataSnapshot.hasChildren()){
                    List<Ciudad> ciudades = new ArrayList<>();
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()){
                        snapshot.child("ciudad").getValue(String.class); //Ver cual nos sirve

                        Ciudad ciudad = snapshot.getValue(Ciudad.class);
                        Log.d(Ciudad.class.getSimpleName(),ciudad.toString());
                        ciudades.add(ciudad);
                    }


                    callback.onSuccess(ciudades);
                } else {
                    callback.onSuccess(new ArrayList<>());
                }

            }else {
                callback.onFailure();
            }
        });
    }


    public interface CiudadesCallback <T> {
        void onSuccess(T data);

        void onFailure();


    }

}
