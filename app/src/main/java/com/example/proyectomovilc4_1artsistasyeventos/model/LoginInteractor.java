package com.example.proyectomovilc4_1artsistasyeventos.model;

import android.content.Context;
import android.util.Log;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.example.proyectomovilc4_1artsistasyeventos.model.database.entities.User;
import com.example.proyectomovilc4_1artsistasyeventos.model.repository.FirebaseAuthRepository;
import com.example.proyectomovilc4_1artsistasyeventos.model.repository.UserRepository;
import com.example.proyectomovilc4_1artsistasyeventos.mvp.LoginMVP;


public class LoginInteractor implements LoginMVP.Model {

    private UserRepository userRepository;
    private FirebaseAuthRepository firebaseAuthRepository;




    public LoginInteractor(Context context) {

        firebaseAuthRepository = FirebaseAuthRepository.getInstance(context);
        userRepository = UserRepository.getInstance(context);


    }


    /* @Override
    public boolean validateCredentials(String email, String password) {

        User user = userRepository.getUserByEmail(email);

        if (user == null) {
            callback.onFailure("Usuario no existe");
        }else if (!user.getPassword().equals(password)){
            callback.onFailure("Contraseña incorrecta");
        }else{
            callback.onSucces();
        }


        //return users.get(email) != null
               // && users.get(email).equals(password);
    } */

    @Override
    public void validateCredentials(String email, String password, ValidateCredentialsCallback callback) {
        firebaseAuthRepository.authenticate(email, password,
                new FirebaseAuthRepository.FirebaseAuthCallback() {
            @Override
            public void onSuccess() {
                callback.onSuccess();
            }

            @Override
            public void onFailure() {
                callback.onFailure("Credenciales invalidas");

            }
        });


        /* try {
            Thread.sleep(15000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }*/
        /* userRepository.getUserByEmail(email, new UserRepository.UserCallback<User>() {

            @Override
            public void onSuccess(User user) {
                if (user== null) {
                    callback.onFailure("Usuario no existe");
                }else if (!user.getPassword().equals(password)){
                    callback.onFailure("Contraseña incorrecta");
                }else{
                    callback.onSuccess();
                }
            }

            @Override
            public void onFailure() {
                callback.onFailure("Error accediendo a fuente de datos");

            }
        });

        userRepository.getAll(new UserRepository.UserCallback<List<User>>(){

            @Override
            public void onSuccess(List<User> data) {
                for (User user:data){
                    Log.d(LoginInteractor.class.getSimpleName(),user.toString());
                }

            }

            @Override
            public void onFailure() {
                Log.w(LoginInteractor.class.getSimpleName(),"Problemas al obtener datos");

            }
        });*/
    }

    @Override
    public boolean isAuthenticated() {
        return firebaseAuthRepository.isAuthenticated();
    }
}
