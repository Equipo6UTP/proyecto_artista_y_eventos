package com.example.proyectomovilc4_1artsistasyeventos.model.database;


import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.example.proyectomovilc4_1artsistasyeventos.model.database.dao.CiudadDao;
import com.example.proyectomovilc4_1artsistasyeventos.model.database.dao.DepartamentoDao;
import com.example.proyectomovilc4_1artsistasyeventos.model.database.dao.GeneroDao;
import com.example.proyectomovilc4_1artsistasyeventos.model.database.dao.UserDao;
import com.example.proyectomovilc4_1artsistasyeventos.model.database.entities.Ciudad;
import com.example.proyectomovilc4_1artsistasyeventos.model.database.entities.Departamento;
import com.example.proyectomovilc4_1artsistasyeventos.model.database.entities.Genero;
import com.example.proyectomovilc4_1artsistasyeventos.model.database.entities.User;

@Database(entities={User.class, Ciudad.class, Genero.class, Departamento.class} , version = 1)//agregue con el primo
public abstract class ArtistasDatabase extends RoomDatabase {


    public abstract UserDao getUserDao();
    public abstract CiudadDao getCiudadDao();//agregue con el P
    public abstract GeneroDao getGeneroDao();
    public abstract DepartamentoDao getDepartamentoDao();

    private static volatile ArtistasDatabase INSTANCE;

    public static ArtistasDatabase getDatabase(Context context){
        if (INSTANCE == null){
            INSTANCE = Room.databaseBuilder(
                    context.getApplicationContext(),
                    ArtistasDatabase.class, "database-name")
                    .allowMainThreadQueries()
                    .build();

        }
        return INSTANCE;
    }

}
