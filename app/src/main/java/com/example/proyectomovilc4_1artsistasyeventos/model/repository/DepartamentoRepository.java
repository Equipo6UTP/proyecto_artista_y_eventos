package com.example.proyectomovilc4_1artsistasyeventos.model.repository;

import android.content.Context;
import android.util.Log;

import com.example.proyectomovilc4_1artsistasyeventos.model.database.ArtistasDatabase;
import com.example.proyectomovilc4_1artsistasyeventos.model.database.dao.CiudadDao;
import com.example.proyectomovilc4_1artsistasyeventos.model.database.dao.DepartamentoDao;
import com.example.proyectomovilc4_1artsistasyeventos.model.database.entities.Ciudad;
import com.example.proyectomovilc4_1artsistasyeventos.model.database.entities.Departamento;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

public class DepartamentoRepository {

    private static DepartamentoRepository instance;

    public static DepartamentoRepository getInstance(Context context){
        if (instance == null){
            instance = new DepartamentoRepository(context);
        }
        return instance;
    }

    private final static Boolean USE_DATABASE = Boolean.FALSE;

    // determinar de donde se va a obtener la info
    private DepartamentoDao departamentoDao;

    private DatabaseReference departamentoRef;

    //private UserDao userRef;


    private DepartamentoRepository(Context context) {
        departamentoDao = ArtistasDatabase.getDatabase(context).getDepartamentoDao();

        //Conexión a base de datos
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        departamentoRef= database.getReference("departamento");

    }

    /*private User getUserByEmailDB(String email) {
        return userDao.getUserByEmail(email);
    }*/

    public void getAll(DepartamentoRepository.DepartamentosCallback<List<Departamento>> callback){
        departamentoRef.get().addOnCompleteListener(task -> {
            if(task.isSuccessful()){
                DataSnapshot dataSnapshot = task.getResult();
                if(dataSnapshot.hasChildren()){
                    List<Departamento> departamentos = new ArrayList<>();
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()){
                        snapshot.child("Departamento").getValue(String.class); //Ver cual nos sirve

                        Departamento departamento = snapshot.getValue(Departamento.class);
                        Log.d(Departamento.class.getSimpleName(),departamento.toString());
                        departamentos.add(departamento);
                    }


                    callback.onSuccess(departamentos);
                } else {
                    callback.onSuccess(new ArrayList<>());
                }

            }else {
                callback.onFailure();
            }
        });
    }


    public interface DepartamentosCallback <T> {
        void onSuccess(T data);

        void onFailure();


    }

}
