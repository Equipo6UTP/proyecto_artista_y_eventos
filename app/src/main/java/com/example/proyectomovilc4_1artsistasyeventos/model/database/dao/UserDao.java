package com.example.proyectomovilc4_1artsistasyeventos.model.database.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.proyectomovilc4_1artsistasyeventos.model.database.entities.User;

import java.util.List;

@Dao
public interface UserDao {

    // Consultar todos los Users
    @Query("SELECT * FROM user")
    List<User> getAll();

    //consulta traer ese usuario por el email
    @Query("SELECT * FROM user WHERE email = :email")
    User getUserByEmail(String email);

    //crear consulta ciudad y genero

    @Query("SELECT * FROM user WHERE city = :city and genre =:genre")
    List<User> getUserByGeneroCiudad(String city, String genre);

    @Insert
    void insert(User...users);

    @Delete
    void delete(User user);
}
