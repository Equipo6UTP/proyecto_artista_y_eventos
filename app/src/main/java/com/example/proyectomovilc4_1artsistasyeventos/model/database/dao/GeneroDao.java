package com.example.proyectomovilc4_1artsistasyeventos.model.database.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.proyectomovilc4_1artsistasyeventos.model.database.entities.Genero;

import java.util.List;

@Dao
public interface GeneroDao {

    // Consultar todos los Users
    @Query("SELECT * FROM genero")
    List<Genero> getAll();

    @Insert
    void insert(Genero...generos);

    @Delete
    void delete(Genero generos);
}
