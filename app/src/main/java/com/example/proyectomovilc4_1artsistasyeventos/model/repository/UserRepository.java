package com.example.proyectomovilc4_1artsistasyeventos.model.repository;

import android.content.Context;
import android.util.Log;

import com.example.proyectomovilc4_1artsistasyeventos.model.database.ArtistasDatabase;
import com.example.proyectomovilc4_1artsistasyeventos.model.database.dao.UserDao;
import com.example.proyectomovilc4_1artsistasyeventos.model.database.entities.User;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

public class UserRepository {

    private static UserRepository instance;

    public static UserRepository getInstance(Context context){
        if (instance == null){
            instance = new UserRepository(context);
        }
        return instance;
    }

    private final static Boolean USE_DATABASE = Boolean.FALSE;

    // determinar de donde se va a obtener la info
    private UserDao userDao;

    private DatabaseReference userRef;

    //private UserDao userRef;


    private UserRepository(Context context) {
        userDao = ArtistasDatabase.getDatabase(context).getUserDao();

        //Conexión a base de datos
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        userRef= database.getReference("user");

        loadInitialDatabase();
    }




    private void loadInitialDatabase() {
        if(USE_DATABASE){
            userDao.insert(
                    new User("a@msn.com","1234"),
                    new User("e@msn.com","4321")
            );
        } else {
            String username = "b@msn.com".replace('@','_').replace('.','_');

            //userRef.setValue("Hola Mundo");
            userRef.child(username).child("email").setValue("b@msn.com");
            userRef.child(username).child("password").setValue("765432");
            //userRef.child("cdiaz").child("password").setValue("12345678");

            username = "c@msn.com".replace('@','_').replace('.','_');
            userRef.child(username)
                    .setValue(new User("c@msn.com","987456"));
        }

    }

    public void getUserByEmail(String email, UserCallback <User>callback) {
        if(USE_DATABASE){
            callback.onSuccess(userDao.getUserByEmail(email));
        } else {
            //Usar Database
            // Read from the database
            String username = email.replace('@','_').replace('.','_');
            userRef.child(username).get()
                    .addOnCompleteListener(task -> {
                        if(task.isSuccessful()) {
                            User value = task.getResult().getValue(User.class);
                            //Log.d(UserRepository.class.getSimpleName(),value.toString());
                            callback.onSuccess(value);
                        } else {
                            callback.onFailure();
                        }
                    });

        }

    }

    //metodo ciudad y genero terminar con el primo
    /*public void getUserGeneroCiudad(String ciudad, String genero, UserCallback <User>callback) {
        if(USE_DATABASE){
            callback.onSuccess(userDao.getUserByGeneroCiudad(ciudad, genero));
        } else {
            //Usar Database
            // Read from the database
            String username = email.replace('@','_').replace('.','_');
            userRef.child(username).get()
                    .addOnCompleteListener(task -> {
                        if(task.isSuccessful()) {
                            User value = task.getResult().getValue(User.class);
                            //Log.d(UserRepository.class.getSimpleName(),value.toString());
                            callback.onSuccess(value);
                        } else {
                            callback.onFailure();
                        }
                    });

        }

    }*/
    //fin metodo

    /*private User getUserByEmailDB(String email) {
        return userDao.getUserByEmail(email);
    }*/

    public void getAll(UserCallback<List<User>> callback){
        userRef.get().addOnCompleteListener(task -> {
            if(task.isSuccessful()){
                DataSnapshot dataSnapshot = task.getResult();
                if(dataSnapshot.hasChildren()){
                    List<User> users = new ArrayList<>();
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()){
                        snapshot.child("email").getValue(String.class); //Ver cual nos sirve

                        User user = snapshot.getValue(User.class);
                        Log.d(UserRepository.class.getSimpleName(),user.toString());
                        users.add(user);
                    }


                    callback.onSuccess(users);
                } else {
                    callback.onSuccess(new ArrayList<>());
                }

            }else {
                callback.onFailure();
            }
        });
    }


    public interface UserCallback <T> {
        void onSuccess(T data);

        void onFailure();


    }

}