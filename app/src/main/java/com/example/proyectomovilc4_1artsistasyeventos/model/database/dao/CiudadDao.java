package com.example.proyectomovilc4_1artsistasyeventos.model.database.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.proyectomovilc4_1artsistasyeventos.model.database.entities.Ciudad;
import com.example.proyectomovilc4_1artsistasyeventos.model.database.entities.User;

import java.util.List;

    @Dao
    public interface CiudadDao {

        // Consultar todos los Users
        @Query("SELECT * FROM ciudad")
        List<Ciudad> getAll();

        @Insert
        void insert(Ciudad...ciudades);

        @Delete
        void delete(Ciudad ciudad);
    }
