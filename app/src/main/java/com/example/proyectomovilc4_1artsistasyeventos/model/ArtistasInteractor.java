package com.example.proyectomovilc4_1artsistasyeventos.model;

import com.example.proyectomovilc4_1artsistasyeventos.model.repository.UserRepository;
import com.example.proyectomovilc4_1artsistasyeventos.mvp.ArtistasMVP;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ArtistasInteractor implements ArtistasMVP.Model {



    private List<ArtistasMVP.ArtistasInfo> data;

    public ArtistasInteractor() {
        data= Arrays.asList(
                new ArtistasMVP.ArtistasInfo("Los Siameses", "Comedia","Cra 1 # 10 - 54, Bogotá D.C., Colombia","Artista Verificado"),
                new ArtistasMVP.ArtistasInfo("Pipe Bueno", "Despecho","Cll 1 # 10 - 20, Medellín, Colombia","Artista Verificado"),
                new ArtistasMVP.ArtistasInfo("Loquillo", "Comedia","Cra 2 # 10 - 10, Cartagena, Colombia","Artista Verificado"),
                new ArtistasMVP.ArtistasInfo("Marbelle", "Tecno-Carrilera","Cra 123 # 10 - 54, Barranquilla, Colombia","Artista Verificado"),
                new ArtistasMVP.ArtistasInfo("Pipe Pelaez", "Vallenato","Cra 3 # 10 - 20, Valledupar, Colombia","Artista Verificado")




        );
    }

    @Override
    public void loadArtistas(loadPaymentsCallback callback) {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        callback.showArtistasInfo(this.data);


    }
}
