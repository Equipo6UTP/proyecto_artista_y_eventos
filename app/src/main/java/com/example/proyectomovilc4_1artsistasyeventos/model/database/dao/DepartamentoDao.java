package com.example.proyectomovilc4_1artsistasyeventos.model.database.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.proyectomovilc4_1artsistasyeventos.model.database.entities.Ciudad;
import com.example.proyectomovilc4_1artsistasyeventos.model.database.entities.Departamento;

import java.util.List;

@Dao
public interface DepartamentoDao {

    // Consultar todos los Users
    @Query("SELECT * FROM departamento")
    List<Departamento> getAll();

    @Insert
    void insert(Departamento...departamentos);

    @Delete
    void delete(Departamento departamento);

}
