package com.example.proyectomovilc4_1artsistasyeventos.model.repository;

import android.content.Context;
import android.util.Log;

import com.example.proyectomovilc4_1artsistasyeventos.model.database.ArtistasDatabase;
import com.example.proyectomovilc4_1artsistasyeventos.model.database.dao.CiudadDao;
import com.example.proyectomovilc4_1artsistasyeventos.model.database.dao.GeneroDao;
import com.example.proyectomovilc4_1artsistasyeventos.model.database.entities.Ciudad;
import com.example.proyectomovilc4_1artsistasyeventos.model.database.entities.Genero;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

public class GeneroRepository {

    private static GeneroRepository instance;

    public static GeneroRepository getInstance(Context context){
        if (instance == null){
            instance = new GeneroRepository(context);
        }
        return instance;
    }

    private final static Boolean USE_DATABASE = Boolean.FALSE;

    // determinar de donde se va a obtener la info
    private GeneroDao generoDao;

    private DatabaseReference generoRef;

    //private UserDao userRef;


    private GeneroRepository(Context context) {
        generoDao = ArtistasDatabase.getDatabase(context).getGeneroDao();

        //Conexión a base de datos
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        generoRef= database.getReference("genero");

    }

    /*private User getUserByEmailDB(String email) {
        return userDao.getUserByEmail(email);
    }*/

    public void getAll(GeneroRepository.GenerosCallback<List<Genero>> callback){
        generoRef.get().addOnCompleteListener(task -> {
            if(task.isSuccessful()){
                DataSnapshot dataSnapshot = task.getResult();
                if(dataSnapshot.hasChildren()){
                    List<Genero> generos = new ArrayList<>();
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()){
                        snapshot.child("genero").getValue(String.class); //Ver cual nos sirve

                        Genero genero = snapshot.getValue(Genero.class);
                        Log.d(Genero.class.getSimpleName(),genero.toString());
                        generos.add(genero);
                    }


                    callback.onSuccess(generos);
                } else {
                    callback.onSuccess(new ArrayList<>());
                }

            }else {
                callback.onFailure();
            }
        });
    }


    public interface GenerosCallback <T> {
        void onSuccess(T data);

        void onFailure();


    }
}
