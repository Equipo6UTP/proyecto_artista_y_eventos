package com.example.proyectomovilc4_1artsistasyeventos.model.repository;

import android.content.Context;

import androidx.annotation.NonNull;

import com.example.proyectomovilc4_1artsistasyeventos.model.database.entities.User;
//import com.google.android.gms.tasks.OnCompleteListener;
//import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class FirebaseAuthRepository {
    private static FirebaseAuthRepository instance;

    public static FirebaseAuthRepository getInstance(Context context){
        if (instance == null){
            instance =new FirebaseAuthRepository(context);
        }
        return instance;
    }

    private UserRepository userRepository;
    private FirebaseAuth firebaseAuth;
    private FirebaseUser currentUser;

    private FirebaseAuthRepository(Context context){
        this.userRepository= UserRepository.getInstance(context);
        this.firebaseAuth = FirebaseAuth.getInstance();
    }

    public boolean isAuthenticated(){

        return getCurrentUser() != null;
    }

    public FirebaseUser getCurrentUser(){
        if(currentUser == null){
            currentUser= firebaseAuth.getCurrentUser();
        }
        return currentUser;
    }
    public void registerNewUSer(User user, FirebaseAuthCallback callback){
        firebaseAuth.signInWithEmailAndPassword(user.getEmail(), user.getPassword())
                .addOnCompleteListener(task -> {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            //Log.d(TAG, "signInWithEmail:success");
                            currentUser = firebaseAuth.getCurrentUser();
                            //TODO Guardar datos adicionales en base de datos
                            callback.onSuccess();
                            //updateUI(user);
                        } else {
                            // If sign in fails, display a message to the user.
                            /*Log.w(TAG, "signInWithEmail:failure", task.getException());
                            Toast.makeText(EmailPasswordActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                            updateUI(null);*/
                            callback.onFailure();
                        }

                });
    }
    public void authenticate(String email, String password, FirebaseAuthCallback callback){
        firebaseAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        // Sign in success, update UI with the signed-in user's information
                        //Log.d(TAG, "signInWithEmail:success");
                        currentUser = firebaseAuth.getCurrentUser();
                        callback.onSuccess();
                        //updateUI(user);
                    } else {
                        // If sign in fails, display a message to the user.
                            /*Log.w(TAG, "signInWithEmail:failure", task.getException());
                            Toast.makeText(EmailPasswordActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                            updateUI(null);*/
                        callback.onFailure();
                    }

        });
    }

    public void logout(FirebaseAuthCallback callback){
        if(currentUser != null){
            firebaseAuth.signOut();
            currentUser = null;
            callback.onSuccess();

        }else{
            callback.onFailure();
        }

    }


    public interface FirebaseAuthCallback{
        void onSuccess();
        void onFailure();
    }

}
