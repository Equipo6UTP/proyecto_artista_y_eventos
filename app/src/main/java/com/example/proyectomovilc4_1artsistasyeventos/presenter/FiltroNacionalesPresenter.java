package com.example.proyectomovilc4_1artsistasyeventos.presenter;

import com.example.proyectomovilc4_1artsistasyeventos.mvp.FiltroDepartamentoMVP;
import com.example.proyectomovilc4_1artsistasyeventos.mvp.FiltroNacionalesMVP;

public class FiltroNacionalesPresenter implements FiltroNacionalesMVP.Presenter {

    private FiltroNacionalesMVP.View view;
    private FiltroNacionalesMVP.Model model;

    public FiltroNacionalesPresenter(FiltroNacionalesMVP.View view) {
        this.view = view;
        this.model = model;
    }
}
