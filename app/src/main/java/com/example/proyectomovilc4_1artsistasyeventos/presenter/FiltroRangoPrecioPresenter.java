package com.example.proyectomovilc4_1artsistasyeventos.presenter;

import com.example.proyectomovilc4_1artsistasyeventos.mvp.FiltroDepartamentoMVP;
import com.example.proyectomovilc4_1artsistasyeventos.mvp.FiltroRangoPrecioMVP;

public class FiltroRangoPrecioPresenter implements FiltroRangoPrecioMVP.Presenter {

    private FiltroRangoPrecioMVP.View view;
    private FiltroRangoPrecioMVP.Model model;

    public FiltroRangoPrecioPresenter(FiltroRangoPrecioMVP.View view) {
        this.view = view;
        this.model = model;
    }
}
