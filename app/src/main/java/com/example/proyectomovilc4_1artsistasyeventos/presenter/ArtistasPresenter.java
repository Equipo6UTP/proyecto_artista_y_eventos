package com.example.proyectomovilc4_1artsistasyeventos.presenter;

import android.os.Bundle;

import com.example.proyectomovilc4_1artsistasyeventos.model.ArtistasInteractor;
import com.example.proyectomovilc4_1artsistasyeventos.mvp.ArtistasMVP;

import java.util.List;

public class ArtistasPresenter implements ArtistasMVP.Presenter {
    private ArtistasMVP.View view;
    private ArtistasMVP.Model model;

    public ArtistasPresenter(ArtistasMVP.View view) {
        this.view = view;
        this.model = new ArtistasInteractor();
    }

    @Override
    public void loadArtistas() {
        view.showProgressBar();
        new Thread(() -> model.loadArtistas(new ArtistasMVP.Model.loadPaymentsCallback() {
            @Override
            public void showArtistasInfo(List<ArtistasMVP.ArtistasInfo> artistasInfo) {
                view.getActivity().runOnUiThread(()->{
                    view.showArtistasInfo(artistasInfo);
                    view.hideProgressBar();
                });

            }
        })).start();

    }

    @Override
    public void onItemSelected(ArtistasMVP.ArtistasInfo info) {
        Bundle params = new Bundle();
        params.putString("name", info.getName());
        params.putString("city", info.getCity());
        view.openLocationActivity(params);
    }
}
