package com.example.proyectomovilc4_1artsistasyeventos.presenter;

import com.example.proyectomovilc4_1artsistasyeventos.mvp.FiltroDepartamentoMVP;
import com.example.proyectomovilc4_1artsistasyeventos.mvp.FiltroGeneroMVP;

public class FiltroGeneroPresenter implements FiltroGeneroMVP.Presenter {

    private FiltroGeneroMVP.View view;
    private FiltroGeneroMVP.Model model;

    public FiltroGeneroPresenter(FiltroGeneroMVP.View view) {
        this.view = view;
        this.model = model;
    }
}
