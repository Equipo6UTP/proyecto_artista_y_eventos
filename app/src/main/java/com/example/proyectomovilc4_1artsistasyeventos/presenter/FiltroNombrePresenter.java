package com.example.proyectomovilc4_1artsistasyeventos.presenter;

import com.example.proyectomovilc4_1artsistasyeventos.mvp.FiltroDepartamentoMVP;
import com.example.proyectomovilc4_1artsistasyeventos.mvp.FiltroNombreMVP;

public class FiltroNombrePresenter implements FiltroNombreMVP.Presenter {

    private FiltroNombreMVP.View view;
    private FiltroNombreMVP.Model model;

    public FiltroNombrePresenter(FiltroNombreMVP.View view) {
        this.view = view;
        this.model = model;
    }
}
