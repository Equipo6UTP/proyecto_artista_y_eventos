package com.example.proyectomovilc4_1artsistasyeventos.presenter;

import com.example.proyectomovilc4_1artsistasyeventos.mvp.FiltroCiudadMVP;
import com.example.proyectomovilc4_1artsistasyeventos.mvp.LoginMVP;

public class FiltroCiudadPresenter implements FiltroCiudadMVP.Presenter {

    private FiltroCiudadMVP.View view;
    private FiltroCiudadMVP.Model model;

    public FiltroCiudadPresenter(FiltroCiudadMVP.View view) {
        this.view = view;
        this.model = model;
    }
}
