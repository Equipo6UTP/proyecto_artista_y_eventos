package com.example.proyectomovilc4_1artsistasyeventos.presenter;

import com.example.proyectomovilc4_1artsistasyeventos.model.LoginInteractor;
import com.example.proyectomovilc4_1artsistasyeventos.mvp.LoginMVP;

public class LoginPresenter implements LoginMVP.Presenter {

    private LoginMVP.View view;
    private LoginMVP.Model model;

    public LoginPresenter(LoginMVP.View view) {
        this.view = view;
        this.model = new LoginInteractor(view.getActivity());
    }

    @Override
    public void loginWithEmail() {
        boolean error = false;

        view.showEmailError("");
        view.showPasswordError("");
        // VALIDATE FIELDS
        LoginMVP.LoginInfo loginInfo = view.getLoginInfo();
        if (loginInfo.getEmail().trim().isEmpty()) {
            view.showEmailError("Correo electronico es obligatorio");
            error = true;
        } else if (!isEmailValid(loginInfo.getEmail().trim())) {
            view.showEmailError("Correo electronico no es valido");
            error = true;
        }

        if (loginInfo.getPassword().trim().isEmpty()) {
            view.showPasswordError("Contraseña es obligatoria");
            error = true;
        } else if (!isPasswordValid(loginInfo.getPassword().trim())) {
            view.showPasswordError("Contraseña no cumple con los criterios de seguridad");
            error = true;
        }

        // validar que el usuario/contraseña sean iguales

        if (!error) {
            view.startWaiting();
            // validar usuario contraseña son corerectos
            //new Thread(() -> {
            model.validateCredentials(
                    loginInfo.getEmail().trim(),
                    loginInfo.getPassword().trim(),
                    new LoginMVP.Model.ValidateCredentialsCallback() {

                        @Override
                        public void onSuccess() {
                            view.getActivity().runOnUiThread(() -> {
                                view.stopWaiting();
                                view.openNewActivity();
                            });
                        }

                        @Override
                        public void onFailure(String error) {
                            view.getActivity().runOnUiThread(() -> {
                                view.stopWaiting();
                                view.showGeneralMessage(error);
                            });

                        }
                    });


        }
    }      // ).start();

    private boolean isEmailValid(String email) {
        return email.contains("@") && email.endsWith(".com");//podemos implementar mas validaciones si se requieren
    }

    private boolean isPasswordValid(String password) {
        return password.length() > 3;//podemos implementar mas validaciones si se requieren
    }

    @Override
    public void loginWithFacebook() {

    }

    @Override
    public void loginWithGoogle() {

    }

    @Override
    public void isAuthenticated() {
        boolean isAuthenticated = model.isAuthenticated();
        if(isAuthenticated){
            view.openNewActivity();
        }
    }
}
