package com.example.proyectomovilc4_1artsistasyeventos.presenter;

import com.example.proyectomovilc4_1artsistasyeventos.mvp.FiltroDepartamentoMVP;
import com.example.proyectomovilc4_1artsistasyeventos.mvp.LoginMVP;

public class FiltroDepartamentoPresenter implements FiltroDepartamentoMVP.Presenter {

    private FiltroDepartamentoMVP.View view;
    private FiltroDepartamentoMVP.Model model;

    public FiltroDepartamentoPresenter(FiltroDepartamentoMVP.View view) {
        this.view = view;
        this.model = model;
    }
}
